#include "BSNode.h"
#define PENTRON 6
//intalize function
BSNode::BSNode(std::string data)
{
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 0;
	this->_data = data;
}

//copy other to this
BSNode::BSNode(const BSNode& other)
{
	this->_left = other._left;
	this->_right = other._right;
	this->_count = other._count;
	this->_data = other._data;
}

//recursive delete function
BSNode::~BSNode()
{
	if (!this->_left && this->_right)
	{
		return;
	}
	else
	{
		this->_left->~BSNode();
		this->_right->~BSNode();
	}
}

//recursive insert function
void BSNode::insert(std::string value)
{
	BSNode* temp = new BSNode(value);
	
	if (this->_data == value)
	{
		this->_count++;
	}

	if (value < this->_data && !this->_left)
	{
		this->_left = temp;
	}
	else if (value > this->_data && !this->_right)
	{
		this->_right = temp;
	}

	if (value < this->_data)
	{
		this->_left->insert(value);
	}
	else if (value > this->_data)
	{
		this->_right->insert(value);
	}

}

BSNode& BSNode::operator=(const BSNode& other)
{
		BSNode equ = BSNode(other);
		return equ;
}

//checks if it has no child
bool BSNode::isLeaf() const
{
	if (this->_left == nullptr && this->_right == nullptr) //if both are null ptrs, its a leaf
	{
		return true;
	}
	else
	{
		return false;
	}
}

std::string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(std::string val) const
{
	if (this == nullptr)
	{
		return false;
	}
	if (val < this->_data)
	{
		this->_left->search(val);
	}
	else if (val > this->_data)
	{
		this->_right->search(val);
	}
	else if (val == this->_data)
	{
		return true;
	}
}

int BSNode::getHeight() const
{
	int lefth;
	int righth;

	if (this == NULL)
	{
		return 0;
	}
	else
	{
		lefth = this->_left->getHeight();
		righth = this->_right->getHeight();
	}
	if (lefth > righth)
	{
		return lefth+1;
	}
	else 
	{
		return righth+1;
	}
}

int BSNode::getDepth(const BSNode& root) const
{
	if (this == nullptr)
	{
		return -1;
	}
	if (root._data > this->_data)
	{
		return this->getDepth(*root._left) + 1;
	}
	else if (root._data < this->_data)
	{
		return this->getDepth(*root._right) + 1;
	}
	else if (root._data == this->_data)
	{
		return 0;
	}
}

void BSNode::printNodes() const
{
	if (this)
	{
		if (this->_left)
		{
			this->_left->printNodes();
		}
		
		std::cout << this->_data + "-";
		std::cout << this->_count << std::endl;

		if (this->_right)
		{
			this->_right->printNodes();
		}
	}

}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	return 0;
}



