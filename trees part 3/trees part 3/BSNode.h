#pragma once

#include <string>
#include <iostream>

template<class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

};