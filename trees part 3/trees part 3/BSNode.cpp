#include "BSNode.h"
#define PENTRON 6
//intalize function
template<class T>
BSNode<T>::BSNode<T>(T data)
{
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 0;
	this->_data = data;
}

//copy other to this
template<class T>
BSNode<T>::BSNode<T>(const BSNode<T>& other)
{
	this->_left = other._left;
	this->_right = other._right;
	this->_count = other._count;
	this->_data = other._data;
}

//recursive delete function
template<class T>
BSNode<T>::~BSNode<T>()
{
	if (!this->_left && this->_right)
	{
		return;
	}
	else
	{
		this->_left->~BSNode<T>();
		this->_right->~BSNode<T>();
	}
}

//recursive insert function
template<class T>
void BSNode<T>::insert(T value)
{
	BSNode<T>* temp = new BSNode<T>(value);
	
	if (this->_data == value)
	{
		this->_count++;
	}

	if (value < this->_data && !this->_left)
	{
		this->_left = temp;
	}
	else if (value > this->_data && !this->_right)
	{
		this->_right = temp;
	}

	if (value < this->_data)
	{
		this->_left->insert(value);
	}
	else if (value > this->_data)
	{
		this->_right->insert(value);
	}

}

//checks if it has no child
template<class T>
bool BSNode<T>::isLeaf() const
{
	if (this->_left == nullptr && this->_right == nullptr) //if both are null ptrs, its a leaf
	{
		return true;
	}
	else
	{
		return false;
	}
}

template<class T>
T BSNode<T>::getData() const
{
	return this->_data;
}

template<class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return this->_left;
}

template<class T>
BSNode<T>* BSNode<T>::getRight() const
{
	return this->_right;
}

template<class T>
bool BSNode<T>::search(T val) const
{
	if (this == nullptr)
	{
		return false;
	}
	if (val < this->_data)
	{
		this->_left->search(val);
	}
	else if (val > this->_data)
	{
		this->_right->search(val);
	}
	else if (val == this->_data)
	{
		return true;
	}
}

template<class T>
int BSNode<T>::getHeight() const
{
	int lefth;
	int righth;

	if (this == NULL)
	{
		return 0;
	}
	else
	{
		lefth = this->_left->getHeight();
		righth = this->_right->getHeight();
	}
	if (lefth > righth)
	{
		return lefth+1;
	}
	else 
	{
		return righth+1;
	}
}

template<class T>
int BSNode<T>::getDepth(const BSNode<T>& root) const
{
	if (this == nullptr)
	{
		return -1;
	}
	if (root._data > this->_data)
	{
		return this->getDepth(*root._left) + 1;
	}
	else if (root._data < this->_data)
	{
		return this->getDepth(*root._right) + 1;
	}
	else if (root._data == this->_data)
	{
		return 0;
	}
}

template<class T>
void BSNode<T>::printNodes() const
{
	if (this)
	{
		if (this->_left)
		{
			this->_left->printNodes();
		}
		
		std::cout << this->_data << "-";
		std::cout << this->_count << std::endl;

		if (this->_right)
		{
			this->_right->printNodes();
		}
	}

}

template<class T>
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode<T>* node) const
{
	return 0;
}



