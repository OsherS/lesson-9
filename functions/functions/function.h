#pragma once

#include <iostream>

template<class T>
int compare(T, T);

template<class T>
void bubbleSort(T arr[], int n);

template<class T>
void printArray(T arr[], int l);

template<class T>
class MyClass
{
public:
	T _foo;
};



template<class T>
inline int compare(T a, T b)
{
	if (a > b)
	{
		return -1;
	}
	else if (a == b)
	{
		return 0;
	}
	else 
	{
		return 1;
	}
	return 0;
}

template<class T>
inline void bubbleSort(T arr[], int n)
{
	T temp;
	int i, j;
	for (i = 0; i < n - 1; i++)
		for (j = 0; j < n - i - 1; j++)
			if (arr[j] > arr[j + 1])
			{
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}

}

template<class T>
inline void printArray(T arr[], int l)
{
	for (int i = 0; i < l; ++i) {
		std::cout << arr[i] << " \n";
	}
}



	


